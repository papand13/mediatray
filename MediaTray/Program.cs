﻿using System;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsInput;
using WindowsInput.Native;

namespace MediaTray
{
    static class Program
    {
        [STAThread]
        private static void Main()
        {
            Application.Run(new MediaTrayApplication());
        }
    }

    public class MediaTrayApplication : Form
    {
        private NotifyIcon trayIcon;
        private ContextMenu trayMenu;
        private InputSimulator simulator;
        private bool singleClickedLeft = false;

        public MediaTrayApplication()
        {
            simulator = new InputSimulator();

            trayMenu = new ContextMenu();
            trayMenu.MenuItems.AddRange(new MenuItem[] {
                new MenuItem("Play/Pause", OnPlayPause),
                new MenuItem("Next", OnNext),
                new MenuItem("Previous", OnPrevious),
                new MenuItem("-"),
                new MenuItem("Exit", OnExit)
            });

            trayIcon = new NotifyIcon
            {
                Icon = Properties.Resources.MediaTrayIcon,
                ContextMenu = trayMenu,
                Visible = true
            };
            trayIcon.MouseDoubleClick += TrayIcon_MouseDoubleClick;
            trayIcon.MouseClick += TrayIcon_MouseClick;
        }

        void TrayIcon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
                simulator.Keyboard.KeyPress(VirtualKeyCode.MEDIA_NEXT_TRACK);
            singleClickedLeft = false;
        }

        void TrayIcon_MouseClick(object sender, MouseEventArgs e)
        {
            ClickHandler(e);
        }

        private void ClickHandler(MouseEventArgs e)
        {
            Task.Run(() =>
            {
                if (e.Button == MouseButtons.Left)
                {
                    singleClickedLeft = true;
                    Task.Delay(100).Wait();
                    if (singleClickedLeft)
                    {
                        simulator.Keyboard.KeyPress(VirtualKeyCode.MEDIA_PLAY_PAUSE);
                        singleClickedLeft = false;
                    }
                }
            });
        }

        protected override void OnLoad(EventArgs e)
        {
            Visible = false;
            ShowInTaskbar = false;
            base.OnLoad(e);
        }

        public void OnExit(object sender, EventArgs e)
        {
            Application.Exit();
        }

        public void OnPlayPause(object sender, EventArgs e)
        {
            simulator.Keyboard.KeyPress(VirtualKeyCode.MEDIA_PLAY_PAUSE);
        }

        public void OnNext(object sender, EventArgs e)
        {
            simulator.Keyboard.KeyPress(VirtualKeyCode.MEDIA_NEXT_TRACK);
        }

        public void OnPrevious(object sender, EventArgs e)
        {
            simulator.Keyboard.KeyPress(VirtualKeyCode.MEDIA_PREV_TRACK);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                trayIcon.Dispose();

            base.Dispose(disposing);
        }
    }
}
